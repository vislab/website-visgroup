function toggleFullBioVisibility(name) {

  var dots = document.getElementById(name + " dots");
  var moreText = document.getElementById(name + " more");
  var btnText = document.getElementById(name + " btnToggleFullBioVisibility");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
} 