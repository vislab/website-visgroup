
function updateFormattedDate() {
  var longDateHolder = document.getElementById("longDateHolder");
  var longDate = longDateHolder.innerHTML;
  var lastUpdated = document.getElementById("lastUpdated");
  const array = longDate.split("T");
  lastUpdated.innerHTML = array[0];
} 

updateFormattedDate();