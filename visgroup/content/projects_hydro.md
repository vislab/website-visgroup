---
title: "Hydrosystem Projects"
description: "at the Visualization Center TESSIN VISLab"
date: 2023-06-07T23:52:18+02:00
---

{{< single-page-intro text="This is a selection of our visualisation studies for hydrosystems:" >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT WETURBAN
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="WetUrban - Model-based management of water extremes"
	image-source="carousel_weturban"
	>}}

{{< project-description >}}

Based on a prototype for the city of Dresden, Germany, a methodology was developed to use digital city models as a basis for a visualisation- and interaction framework for multiple coupled hydrological and hydrodynamical process models. Specifically, we coupled a surface run-off model simulating flooding (using BASEMENT), a simulation of the sewage network (using SWMM) and a groundwater simulation (using OpenGeoSys) via open python-based interfaces. Simulation results are shown in context of a 3d city model consisting of 3d surfaces, building models, land use classes as well as additional data such as streams, groundwater wells, etc.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Coupled systems for the city of Dresden, Germany](https://www.youtube.com/watch?v=VldRMYdbdSA)

{{< project-subheadline title="Links" >}}
[WetUrban](https://www.ufz.de/index.php?en=49390)

{{< project-subheadline title="Publications" >}}

* Ö O Şen, L Backhaus, S Farrokhzadeh, et al. (2023): *A Visual-Scenario-Based Environmental Analysis Approach to the Model-Based Management of Water Extremes in Urban Regions.* Proc. of Workshop on Visualisation in Environmental Sciences (EnvirVis), pp. 51–60. The Eurographics Association, 2023. [DOI:10.2312/envirvis.20231106](https://diglib.eg.org/xmlui/handle/10.2312/envirvis20231106)
* L Backhaus, K Rink, D Novoa (2023): *[WetUrban - Modellbasiertes Management von Wasserextremen in urbanen Regionen](https://elibrary.vdi-verlag.de/10.51202/1438-5716-2023-5/wwt-wasserwirtschaft-wassertechnik-jahrgang-72-2023-heft-5).* wwt Wasserwirtschaft Wassertechnik 72(5), pp. 10-14.

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT MOSES
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="MOSES-Müglitz: Monitoring and Modelling Campaign"
	image-source="carousel_moses"
	>}}

{{< project-description >}}

The hydrological study in the Müglitz catchment is part of the MOSES initiative ([Modular Observation Solutions for Earth Systems](https://www.ufz.de/moses), coordinated by UFZ) investigating the impact of extreme meteorological events on local hydrological processes. The presented visualization study is also a result of a close collaboration of the ESM and Digital Earth initiatives, combining the expertise in monitoring and modelling environmental processes in a Virtual Reality framework.

{{< /project-description >}}

{{< project-subheadline title="Videos" >}}
* [Müglitz Catchment Demo](https://www.youtube.com/watch?v=plPEtkkR0pQ)
* [Presentation @ EuroVis 21](https://www.youtube.com/watch?v=iIRBpXOWuPg&t=2716s)

{{< project-subheadline title="Links" >}}
[MOSES](https://www.ufz.de/moses), [ESM](https://www.esm-project.net/), [Digital Earth](https://www.digitalearth-hgf.de/)

{{< project-subheadline title="Publications" >}}

* K Rink, Ö O Şen, M Hannemann, et al.: *A Virtual Geographic Environment for the Exploration of Hydro-Meteorological Extremes.* Proc. of Workshop on Visualisation in Environmental Sciences (EnvirVis), pp. 51–59. The Eurographics Association, 2021. [DOI:10.2312/envirvis.20211084](https://diglib.eg.org/bitstream/handle/10.2312/envirvis20211084/051-059.pdf)
* K Rink, Ö O Şen, M Hannemann, et al. (2022): *An Environmental Exploration System for Visual Scenario Analysis of Regional Hydro-Meteorological Systems.* Comput Graph 103, pages 192-200. [DOI:10.1016/j.cag.2022.02.009](https://www.sciencedirect.com/science/article/abs/pii/S0097849322000309)

{{< project-end >}}



<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT POYANG
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
    image-source="carousel_poyang"
	project-title="Monitoring and Modelling in Large River-Lake Systems"
	>}}

{{< project-description >}}

A virtual geographic environment for the catchment of Poyang Lake, China, with a size of 162.000 km<sup>2</sup>. The representation of rivers or lakes is enhanced by a wide range of observation data sets such as water levels or contaminant loads. Simulation results of the groundwater and the water level of flow paths of the lake itself are integrated for a holistic analysis of water and solute dynamics.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Poyang Lake Basin Demo](https://www.youtube.com/watch?v=7r282lIGsHk)

{{< project-subheadline title="Publications" >}}
* K Rink, E Nixdorf, C Zhou, et al. (2020): *A Virtual Geographic Environment for Multi-Compartment Water and Solute Dynamics in Large Catchments.* J Hydrol 582, art. 124507. [DOI:10.1016/j.jhydrol.2019.124507](http://dx.doi.org/10.1016/j.jhydrol.2019.124507)
* C Yan, K Rink, L Bilke, et al. (2020): *Virtual geographical environment-based environmental information system for Poyang Lake Basin.* In: Chinese water systems. Volume 3: Poyang Lake Basin, pp. 293-310. Springer, Cham. [DOI:10.1007/978-3-319-97725-6_18](http://dx.doi.org/10.1007/978-3-319-97725-6_18)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT OMAN
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
    image-source="carousel_oman"
	project-title="Coastal Water Management"
	>}}

{{< project-description >}}

Salt water intrusion due to overexploitation in coastal areas is deteriorating groundwater resources and heavily affecting urban areas. A suitable visualisation of the subsurface flow systems is designed to support the development of protection measures.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Salt Water Intrusion Demo](https://www.youtube.com/watch?v=-xBQJ9WWPJY)

{{< project-subheadline title="Links" >}}
[IWAS-OMAN](https://www.ufz.de/iwas-sachsen/index.php?en=18029)

{{< project-subheadline title="Publications" >}}
M Walther, L Bilke, J-O Delfs, et al. (2014): *Assessing the saltwater remediation potential of a three-dimensional, heterogeneous, coastal aquifer system.* Environ Earth Sci 72, 3827–3837. [DOI:10.1007/s12665-014-3253-2](http://dx.doi.org/10.1007/s12665-014-3253-2)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT DEAD SEA
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
    image-source="carousel_deadsea"
	project-title="Groundwater Balance in Arid Regions"
	>}}

{{< project-description >}}

Numerical simulation of the water balance for the Dead Sea, showing the decrease in the natural replenishment rate of groundwater due to water used for agriculture and reduced precipitation due to climate change.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Western Dead Sea Escarpment](https://www.youtube.com/watch?v=bj6lticy6jA)

{{< project-subheadline title="Links" >}}
[Sustainable Management of Water Resources (Quantity and Quality) in the Dead Sea Area (SUMAR)](https://www.ufz.de/index.php?en=35277)

{{< project-subheadline title="Publications" >}}
A Gräbe, T Rödiger, K Rink, et al. (2013): *Numerical analysis of the groundwater regime in the western Dead Sea escarpment, Israel + West Bank.* Environ Earth Sci 69(2), 571-585. [DOI:10.1007/s12665-012-1795-8](http://dx.doi.org/10.1007/s12665-012-1795-8)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT AMMER
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
    image-source="carousel_ammer"
	project-title="Recharge and Discharge Controls on Groundwater Travel Times"
	>}}

{{< project-description >}}

Travel times and flow paths of groundwater from its recharge area to drinking-water production wells will govern how the quality of pumped groundwater responds to contaminations. Simulating different recharge scenarios and fitting to observed water levels allows understanding the groundwater regime in the area and to estimate water quality for production.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Groundwater Flow in Ammer Catchment](https://www.youtube.com/watch?v=bKxT-w0FCEQ)

{{< project-subheadline title="Links" >}}
Water and Earth System Science (WESS)

{{< project-subheadline title="Publications" >}}
* B Selle, K Rink, O Kolditz (2013): *Recharge and discharge controls on groundwater travel times and flow paths to production wells for the Ammer catchment in SW Germany.* Environ Earth Sci 69(2), 443-452. [DOI:10.1007/s12665-013-2333-z](http://dx.doi.org/10.1007/s12665-013-2333-z)
* K Rink, T Fischer, B Selle, O Kolditz (2013): *A Data Exploration Framework for Validation and Setup of Hydrological Models.* Environ Earth Sci 69(2), 469-477. [DOI:10.1007/s12665-012-2030-3](http://dx.doi.org/10.1007/s12665-012-2030-3)

{{< project-end >}}
