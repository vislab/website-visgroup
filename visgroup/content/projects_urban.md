---
title: "Urban System Projects"
description: "at the Visualization Center TESSIN VISLab"
date: 2023-06-07T23:52:18+02:00
---

{{< single-page-intro text="Here is a selection of our visualisation studies for urban systems:" >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT ANGUS II
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Energy system infrastructure and storage"
	image-source="carousel_angus"
	>}}

{{< project-description >}}

An environmental information system for the state of Schleswig Holstein, Germany, has been set up, integrating GIS data on renewable energy sources and infrastructure, topographical models, and subsurface information for an overview of the current state of renewable energy production. Simulation results for a compressed air energy storage in the stratigraphic formations and an aquifer thermal energy storage for a district of the city of Kiel demonstrate options for large scale energy storage in the subsurface. Adding the spatially discritised heat demand completes the overview of the current energy system infrastructure. 

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Schleswig-Holstein Energy Systems Demo](https://www.youtube.com/watch?v=NHx8ZrO6VuE)

{{< project-subheadline title="Project Links" >}}
[ANGUS II](https://www.angus2.de)

{{< project-subheadline title="Publications" >}}

* Rink, K, ÖO Şen, M Schwanebeck, et al. (2022): *An Environmental Information
System for the Exploration of Energy Systems*. Geotherm Energ 10, art. 4. [DOI:10.1186/s40517-022-00215-5](https://geothermal-energy-journal.springeropen.com/articles/10.1186/s40517-022-00215-5)

{{< project-end >}}



<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT URBAN CATCHMENTS
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_chaohu"
	project-title="Urban Infrastucture and Water Systems"
	>}}

{{< project-description >}}

Data and model integration for urban water systems of Chaohu Lake, China and the city of Chaohu itself. Included are a wide range of environmental data as land use, streams, channels, the lake, as well as online monitoring systems. Integrated into the resulting 3D scene are results from numerical simulations of the Lake (via GETM), the groundwater within the lake catchment (via OpenGeoSys) and the sewage system and storm water channels in the city of Chaohu (via SWMM).
{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Urban Catchments Demo](https://www.youtube.com/watch?v=kjVold9M6yM)

{{< project-subheadline title="Project Links" >}}
[Urban Catchments](https://www.ufz.de/urbancatchments/)

{{< project-subheadline title="Publications" >}}
* A. Sachse, Z. Liao, W. Hu, et al. (2019): *Chinese Water Systems – Volume 2: Managing
Water Resources for Urban Catchments: Chaohu.* Springer, Cham. [ISBN: ISBN: 978-3-319-97567-2](https://link.springer.com/book/10.1007/978-3-319-97568-9)
* Rink, K., C. Chen, L. Bilke, et al. (2018). *Virtual geographic environments
for water pollution control.* Int J Dig Earth 11 (4), pp. 397–407. [DOI:10.1080/17538947.2016.1265016](https://www.doi.org/10.1080/17538947.2016.1265016)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT TAUCHA
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_taucha"
	project-title="Shallow Geothermal Energy Systems"
	>}}

{{< project-description >}}

Visualisation for the planning of ground source heat pump systems for heating and cooling supply of a new residential district in the North of Leipzig, Germany. Included are representations of subsurface structures acquired by geophysical exploration in combination with borehole heat exchangers and the simulated groundwater level.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
[Shallow Geothermal Systems](https://www.youtube.com/watch?v=2w5lCF3GOCg), [Geothermal Systems Chaohu](https://www.youtube.com/watch?v=ZKtNLhI-ZNQ)

{{< project-subheadline title="Project Links" >}}
[SAGS](https://www.ufz.de/index.php?en=46270), [ANGUS+](http://www.ufz.de/index.php?en=37525), [EASyQuart](http://www.ufz.de/index.php?en=47017)

{{< project-subheadline title="Publications" >}}
* Vienken, T., S. Schelenz, K. Rink, and P. Dietrich (2015): *Sustainable Intensive
Thermal Use of the Shallow Subsurface – A Critical View on the Status
Quo.* Groundwater 53 (3), pp. 356–361. [DOI:10.1111/gwat.12206](http://onlinelibrary.wiley.com/doi/10.1111/gwat.12206/abstract)
* S Chen, F Witte, O Kolditz, H Shao (2020): *Shifted thermal extraction rates in large Borehole Heat Exchanger array – A numerical experiment.* Applied Thermal Engineering 167, art. 114750. [DOI:10.1016/j.applthermaleng.2019.114750](https://doi.org/10.1016/j.applthermaleng.2019.114750)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT BAYRISCHER BAHNHOF
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_bayrischerbahnhof"
	project-title="Urban Climate"
	>}}

{{< project-description >}}

The influence of the absence or presence of buildings on urban microclimate is simulated and depicted in the actual building plan for a quarter within the city of Leipzig, Germany, including simulated wind directions and their influence on temperatures at different elevations.

{{< /project-description >}}

{{< project-subheadline title="Publications" >}}

* F Koch, L Bilke, C Helbig, U Schlink (2018): *Compact or cool? The impact of brownfield redevelopment on inner-city micro climate.* Sustainable Cities and Society 38, 31-41. [DOI:10.1016/j.scs.2017.11.021](http://dx.doi.org/10.1016/j.scs.2017.11.021)


{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT WADI SHUEIB
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_wadishueib"
	project-title="Waste Water Treatment"
	>}}

{{< project-description >}}

Options for centralised vs decentralised waste water systems for small villages in Jordan, displayed in the 3D geographical context. Integrated are a wide range GIS data sets for various scenarios for fresh water and sewage systems for cost evaluation based on high-resolution digital elevation models.
{{< /project-description >}}

{{< project-subheadline title="Project Links" >}}
[Urban Catchments](https://www.ufz.de/urbancatchments), [SMART-MOVE](hhttp://www.iwrm-smart-move.de)

{{< project-end >}}
