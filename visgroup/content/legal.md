---
title: "Legal Statements"
date: 2023-06-07T22:55:13+02:00
---


## Privacy Policy

This web site does not collect, store or process any user data. It does not use web analytics or cookies and even web server logs are fully anonymous.

## Copyright and License

Unless otherwise stated, the contents of this website are copyright © 2023 by the Data Integration and Visualisation Group members. Notable exceptions include research papers, where you should contact the authors for relevant information.
    
Note that this list does not claim to be complete. Moreover, this website contains resources from a number of third-party projects, which belong to the authors of those external projects and are covered by the respective upstream licenses.

All information under the default copyright is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.


## Third-Party Code

This website uses Hugo as a static website generator

## Contact Us

In case of questions about the website or any of its contents, please contact us at vislab@ufz.de.

## Impressum

Verantwortlich für diese Seite im Sinne des Presserechts:

Prof. Dr. Olaf Kolditz, Helmholtz-Zentrum für Umweltforschung GmbH - UFZ, Permoserstr. 15, 04318 Leipzig.


## Hinweis für Benutzer in Deutschland

Die Autoren der OpenGeoSys-Website übernehmen keinerlei Garantie für die Richtigkeit, Vollständigkeit und Aktualität der bereitgestellten Informationen. Insbesondere übernehmen die Autoren keinerlei Haftung für Inhalte, die als fremde Inhalte gekennzeichnet und sind nicht dafür verantwortlich, dass solche Inhalte vollständig, aktuell, richtig und rechtmässig sind und nicht in unzulässiger Weise in die Rechte Dritter eingreifen. Dies gilt insbesondere auch für externe Webseiten, auf die durch einen Link verwiesen wird.
