---
featured_image: general/header-bg-small.jpg
site_logo: general/logo.jpg
description: Helmholtz Centre for Environmental Research GmbH – UFZ
---

<!-------------------------------------------------------->
<!------------------------ Intro ------------------------->
<!-------------------------------------------------------->

{{< single-page-intro text="The Visualisation Center TESSIN VISLab (Vislab) allows scientists with different backgrounds to explore and analyse complex and heterogeneous spatial data sets. An interactive stereoscopic video wall is used to dive right into the displayed environmental data sets, supporting visitors in understanding scientific questions and challenges. The VisLab fosters knowledge transfer in the scientific community as well as among the general public." >}}


<!-------------------------------------------------------->
<!----------------------- Projects ----------------------->
<!-------------------------------------------------------->

{{< subpage-preview-start
	title="Projects"
	anchor-id="anchor-projects"
	background="#f5f5f5"
	>}}

We work on a variety of visualisation and data integration projects in different fields of application:

{{< project-category-preview
	imagePosition="left"
	image="project_categories/cat_hydro.jpg"
	title="Hydrosystems"
	text="Studies focussing on hydrological or hydrogeological process such as groundwater flow and -managment, groundwater/surface water interaction, lakes, and coastal systems."
	link="[Hydrosystems]( {{< ref \"projects_hydro.md\" >}} )"
 	>}}

{{< project-category-preview
	imagePosition="left"
	image="project_categories/cat_urban.jpg"
	title="Urban Systems"
	text="Studies related to the resource management in urban areas, including energy infrastructures, energy storage, or sewage systems."
	link="[Urban Systems]( {{< ref \"projects_urban.md\" >}} )"
	>}}

{{< project-category-preview
	imagePosition="left"
	image="project_categories/cat_geotechnics.jpg"
	title="Energy & Geotechnics"
	text="Geotechnical studies with a wide range of applications in energy storage, waste management as well as deep and shallow geothermal systems."
	link="[Energy & Geotechnics]( {{< ref \"projects_geo.md\" >}} )"
	>}}

{{< project-category-preview
	imagePosition="left"
	image="project_categories/cat_climate.jpg"
	title="Climate & Eco Systems"
	text="Applications focussing of weather, climate and biodiversity simulations, including the visualisation of extreme weather events, forest growth, or urban micro climates."
	link="[Climate & Eco Systems]( {{< ref \"projects_climate.md\" >}} )"
	>}}



{{< subpage-preview-end >}}


<!-------------------------------------------------------->
<!--------------- Data Integration ----------------------->
<!-------------------------------------------------------->

{{< subpage-preview-start
	title="Data Integration"
	anchor-id="anchor-dataintegration"
	background="#E0F4C8"
	>}}

Adjusting heterogeneous data sets to fit into a unified geographic context, is an essential step for subsequent tasks such as setting up numerical models or preparing scientific visualisation studies.

{{< subpage-preview-end
	link="[See our workflows for data integration]( {{< ref \"dataIntegration.md\" >}} )"
	>}}


<!-------------------------------------------------------->
<!---------------------- The Team ------------------------>
<!-------------------------------------------------------->

{{< subpage-preview-start
	title="The Team"
	anchor-id="anchor-team"
	background="#f5f5f5"
	>}}

The Data Integration and Visualisation Group is led by Karsten Rink and consists of members with different research interests that complement each other when working on complex data processing and visualisation projects.

{{< team-gallery >}}

{{< one-team-member-small
	name="Dr. Karsten Rink"
	image="team/karsten.jpg"
	anchor-link="team#anchor-karsten"
	>}}

{{< one-team-member-small
	name="Lars Bilke"
	image="team/lars.jpg"
	anchor-link="team#anchor-lars"
	>}}

{{< one-team-member-small
	name="Özgür Ozan Şen"
	image="team/ozan.jpg"
	anchor-link="team#anchor-ozan"
	>}}

{{< one-team-member-small
	name="Nico Graebling"
	image="team/nico.jpg"
	anchor-link="team#anchor-nico"
	>}}

{{< /team-gallery >}}

{{< team-gallery >}}

{{< one-team-member-small
	name="Julian Heinze"
	image="team/julian.jpg"
	anchor-link="team/#anchor-julian"
	>}}

{{< one-team-member-small
	name="Susann Goldstein"
	image="team/susann.jpg"
	anchor-link="team/#anchor-susann"
	>}}
	
{{< one-team-member-small
	name="Markus Jahn"
	image="team/markus.jpg"
	anchor-link="team/#anchor-markus"
	>}}	
{{< /team-gallery >}}
<br/>

{{< subpage-preview-end
	link="[Get to know the team]( {{< ref \"team.md\" >}} )"
	>}}


<!-------------------------------------------------------->
<!---------------- The Way We Work ----------------------->
<!-------------------------------------------------------->

{{< subpage-preview-start
	title="The Way We Work"
	anchor-id="anchor-waywework"
	background="#e2f8fc"
	>}}

Our projects are from a wide range of applications. Therefore, we are working in close collaboration with domain scientists providing data to get an idea of their research and to discuss their aims. With this information in mind, the multidisciplinary team of domain scientists and visualisation experts create the visualisation. In this way, we assure that the result of our work is useful for researchers to gain further insight into their data or to communicate their work to stakeholders or the public.

{{< subpage-preview-end
	link=""
	>}}


<!-------------------------------------------------------->
<!------------------- Publications ----------------------->
<!-------------------------------------------------------->


{{< subpage-preview-start
	title="Publications"
	anchor-id="anchor-publications"
	background="#f5f5f5"
	>}}

{{< publication-list limit=5  description=0 margin-left="0px" >}}

{{< subpage-preview-end
	link="[See more publications]( {{< ref \"publications.md\" >}} )"
	>}}
