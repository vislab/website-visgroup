---
title: "Energy Systems & Geotechnics"
description: "at the Visualization Center TESSIN VISLab"
date: 2023-06-07T23:52:18+02:00
---

{{< single-page-intro text="This is a selection of our visualisation studies for hydrosystems:" >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT MT TERRI
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Underground Research Laboratories"
	image-source="carousel_mtterri"
	>}}

{{< project-description >}}

Underground research laboratories such as in Mont Terri are essential for intensive in-situ research on the storage of radioactive waste, to better understand processes and phenomena related to the usage of subsurface repositories. We have developed a prototype for an experimental information system for the Mont Terri URL, integrating geological information with CAD data, observation results, and numerical simulation results predicting the development of planned or ongoing experiments.
{{< /project-description >}}

{{< project-subheadline title="Videos" >}}

* [Virtual Tour, for details see Graebling et al. 2023](https://youtu.be/kH34J9cZ3aI)
* [Presentation @ EuroGraphics/EuroVis 2020](https://youtu.be/I1c8E77FEz0)
* [Prototype for a visual data management system](https://youtu.be/VH9I1iaeAiM)

{{< project-subheadline title="Project Links" >}}
[GeomInt](https://www.ufz.de/geomint/), [iCross](https://www.ufz.de/index.php?en=46097)

{{< project-subheadline title="Publications" >}}

* N Graebling et al. (2023): *Feels like an Indie Game – Evaluation of a Virtual Field Trip Prototype on Radioactive Waste Management Research for University Education.* in IEEE Computer Graphics and Applications. [DOI:10.1109/MCG.2023.3328169](https://doi.org/10.1109/MCG.2023.3328169)
* N Graebling, Ö O Şen, L Bilke, et al. (2022): *Prototype of a Virtual Experiment Information System for the Mont Terri Underground Research Laboratory.* Front Earth Sci 10, art. 946627. [DOI:10.3389/feart.2022.946627](https://doi.org/10.3389/feart.2022.946627)
* N Graebling, O Kolditz, K Rink (2021): *VR Task: Development of a prototype (CD-A) for visual data- and model integration in the Mont Terri Rock Laboratory.*  [The Mont Terri Project](https://www.mont-terri.ch), Technical Report 2021-44.
* K Rink, L Bilke, F Raith, et al. (2020): *A Virtual Exploration of the Underground Rock Laboratory Mont Terri.* EUROVIS 2020 WISDOME Contest.
* P Bossart and A G Milnes (2018): [*Twenty years of reserach at the Mont Terri rock laboratory: what we have learnt.*]((https://link.springer.com/chapter/10.1007/978-3-319-70458-6_22)) Swiss Journal of Geosciences. Birkhäuser Cham. [DOI:10.1007/978-3-319-70458-6](https://doi.org/10.1007/978-3-319-70458-6)


{{< project-subheadline title="Credits" >}}
To Swisstopo, the Mt. Terri Consortium, BGR, and the Helmholtz Association for continuous support.  
GeomInt (BMBF grant 03G0866A), iCROSS (BMBF grant 02NUK053E, Helmholtz Association grant SO-093)
	
{{< project-end >}}


<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT ENERGY STORAGE
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Energy Storage"
	image-source="carousel_saltcaverns"
	>}}

{{< project-description >}}

Large underground stores are required for the storage of fluctuating renewable energy resources. In this project, the frequent loading and unloading of salt cavern arrays for gas storage is investigated from a geomechanical point of view.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
https://www.youtube.com/watch?v=_BtvH2kBt_4

{{< project-subheadline title="Project Links" >}}
[ANGUS+](http://angusplus.de/), [H2-UGS](https://www.ufz.de/index.php?en=46260)

{{< project-subheadline title="Publications" >}}

* A Kabuth, A Dahmke, C Beyer, et al. (2017): *Energy storage in the geological subsurface: dimensioning, risk analysis and spatial planning: the ANGUS+ project.* Environ Earth Sci 76, art. 23. [DOI:10.1007/s12665-016-6319-5](https://link.springer.com/article/10.1007%2Fs12665-016-6319-5)
* N Böttcher, U-J Görke, O Kolditz, T Nagel (2017): *Thermo-mechanical investigation of salt caverns for short-term hydrogen storage.* Environ Earth Sci 76, art. 98. [DOI:10.1007/s12665-017-6414-2](https://link.springer.com/article/10.1007/s12665-017-6414-2)
* M Nolde, M Schwanebeck, F Dethlefsen, et al. (2016): *Utilization of a 3D webGIS to support spatial planning regarding underground energy storage in the context of the German energy system transition at the example of the federal state of Schleswig–Holstein.* Environ Earth Sci 75, art. 1284. [DOI:10.1007/s12665-016-6089-0](https://link.springer.com/article/10.1007/s12665-016-6089-0)

{{< project-subheadline title="Credits" >}}
To the ANGUS team, to BMWi, BMBF for funding

	
{{< project-end >}}


<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT ENHANCED GEOTHERMAL SYSTEMS
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Enhanced Geothermal Systems"
	image-source="carousel_egs"
	>}}

{{< project-description >}}

Enhanced geothermal systems (EGS) are being developed for tapping deep geothermal energy resources. Numerical modelling of the thermo-hydro-mechanical and even chemical processes helps for planning and assessing the environmental  impacts of employing these systems.

{{< /project-description >}}

{{< project-subheadline title="Video" >}}
https://www.youtube.com/watch?v=L5FL62lyLhQ

{{< project-subheadline title="Project Links" >}}
[GEMex](https://www.ufz.de/index.php/index.php?en=49475), POF-3 Groß Schönebeck

{{< project-subheadline title="Publications" >}}

* F Parisio, V Vilarrasa, W Wang, et al. (2017): *The risks of long-term re-injection in supercritical geothermal systems.* Nat Commun 10 (1), art. 4391 [DOI:10.1038/s41467-019-12146-0](https://doi.org/10.1038/s41467-019-12146-0)

{{< project-subheadline title="Credits" >}}
To the GFZ for collaboration and Helmholtz Association for funding within POF-3

	
{{< project-end >}}


<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT Thermo-Chemical Heat Storage (NUMTHECHSTORE)
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Thermo-Chemical Heat Storage (NUMTHECHSTORE)"
	image-source="carousel_numthechstore"
	>}}

{{< project-description >}}

Thermal energy storage is of high strategic relevance for a sustainable energy system. The development of next generation storage systems like thermochemical solutions is accompanied by major scientific challenges. Due to the complexity of the considered storage systems and the exceptional efforts for the development of storage materials as well as for the implementation of large-scale experiments, modelling and numerical simulation are of outstanding importance for the prediction of the operational behaviour and the optimisation of thermochemical heat storage systems.

{{< /project-description >}}

{{< project-subheadline title="Videos" >}}

* https://www.youtube.com/watch?v=W_IVa33uhcI  
* https://www.youtube.com/watch?v=4e3inydE4zY

{{< project-subheadline title="Project Link" >}}
[NUMTHECHSTORE](https://www.ufz.de/index.php?en=37528)

{{< project-subheadline title="Publications" >}}

* T Nagel, S Beckert, C Lehmann et al. (2016): *Multi-physical continuum models of thermochemical heat storage and transformation in porous media and powder beds — A review.* Appl Energ 178, 323-345. [DOI:10.1016/j.apenergy.2016.06.051](https://doi.org/10.1016/j.apenergy.2016.06.051)

{{< project-subheadline title="Credits" >}}
To Helmholtz Association for funding

	
{{< project-end >}}


<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT CARBON CAPTURE STORAGE
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Carbon Capture Storage"
	image-source="carousel_otway"
	>}}

{{< project-description >}}

Geophysical (in particular seismic) exploration provides a better picture of subsurface systems and allows for better planning of geological CO2 storage repositories.

{{< /project-description >}}


{{< project-subheadline title="Project Link" >}}
[PROTECT, CO2BENCH](https://www.ufz.de/index.php?en=46271),

{{< project-subheadline title="Publications" >}}

* C M Krawczyk, D C Tanner, A Henk, et al.(2015): *Seismic and sub-seismic deformation pre­dic­tion in the context of geological carbon trapping and storage.* In: Geological Storage of CO2 – Long Term Security Aspects, Springer, Cham.  pp 97–113. [DOI:10.1007/978-3-319-13930-2_5](https://doi.org/10.1007/978-3-319-13930-2_5)

{{< project-subheadline title="Credits" >}}
To the BMBF for funding within the Geotechnologies Program
	
{{< project-end >}}
