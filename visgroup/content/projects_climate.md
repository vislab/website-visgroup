---
title: "Climate and Ecosystems"
description: "at the Visualization Center TESSIN VISLab"
date: 2023-06-07T23:52:18+02:00
---

{{< single-page-intro text="Here is a selection of our visualisation studies for climate- and ecosystem-related applications:" >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT MEVA
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_meva"
	project-title="Multifaceted Meteorological Data"
	>}}

{{< project-description >}}

Visualisation of multifaceted meteorological data (wind magnitude/direction, cloud mass fractions, temperature, heat flux, etc.) integrated into geographical context (e.g. topography) and time-series data acquired from observation sites. The application supports multiple presentation devices, a user-friendly interface, and allows for cooperative work between researchers of various scientific domains.

{{< /project-description >}}

{{< project-subheadline title="Videos" >}}
[MEVA Demo](https://www.youtube.com/watch?v=2uU2QKTAHYY)

{{< project-subheadline title="Publications" >}}
* C Helbig, L Bilke, H-S Bauer, et al. (2015): *MEVA - An Interactive Visualization Application for Validation of Multifaceted Meteorological Data with Multiple 3D Devices.* PLoS ONE 10(4):e0123811. [DOI::10.1371/journal.
pone.0123811](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0123811)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT FORMIND
----------------------------------------------------------------------
---------------------------------------------------------------------->

{{< project-start
	project-title="Forest Structure Modelling"
	image-source="carousel_formind"
	>}}

{{< project-description >}}

Visualisation of a forest gap model applied to a tropical forest site to provide answers to questions related to species richness, natural and anthropogenic disturbances, wild fires, and carbon balance.
{{< /project-description >}}

{{< project-subheadline title="Project Links" >}}
[FORMIND](http://formind.org/)

{{< project-end >}}

<!--------------------------------------------------------------------
----------------------------------------------------------------------
					START OF PROJECT BAYRISCHER BAHNHOF
----------------------------------------------------------------------
---------------------------------------------------------------------->


{{< project-start
	image-source="carousel_bayrischerbahnhof"
	project-title="Urban Climate"
	>}}

{{< project-description >}}

The influence of the absence or presence of buildings on urban microclimate is simulated and depicted in the actual building plan for a quarter within the city of Leipzig, Germany including simulated wind directions and their influence on temperatures at different elevations.

{{< /project-description >}}

{{< project-subheadline title="Publications" >}}

* F Koch, L Bilke, C Helbig, U Schlink (2018): *Compact or cool? The impact of brownfield redevelopment on inner-city micro climate.* Sustainable Cities and Society 38, 31-41. [DOI:10.1016/j.scs.2017.11.021](http://dx.doi.org/10.1016/j.scs.2017.11.021)

{{< project-end >}}
