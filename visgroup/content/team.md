---
title: "Team"
date: 2023-06-07T22:49:27+02:00
---
{{< team-gallery >}}

{{< one-team-member-small
	name="Karsten Rink"
	image="team/karsten.jpg"
	anchor-link="team/#anchor-karsten"
	>}}

{{< one-team-member-small
	name="Lars Bilke"
	image="team/lars.jpg"
	anchor-link="team/#anchor-lars"
	>}}

{{< one-team-member-small
	name="Özgür Ozan Şen"
	image="team/ozan.jpg"
	anchor-link="team/#anchor-ozan"
	>}}

{{< one-team-member-small
	name="Nico Graebling"
	image="team/nico.jpg"
	anchor-link="team/#anchor-nico"
	>}}

{{< /team-gallery >}}

{{< team-gallery >}}

{{< one-team-member-small
	name="Julian Heinze"
	image="team/julian.jpg"
	anchor-link="team/#anchor-julian"
	>}}

{{< one-team-member-small
	name="Susann Goldstein"
	image="team/susann.jpg"
	anchor-link="team/#anchor-susann"
	>}}
	
{{< one-team-member-small
	name="Markus Jahn"
	image="team/markus.jpg"
	anchor-link="team/#anchor-markus"
	>}}	
{{< /team-gallery >}}

{{< single-page-intro text="The Data Integration and Visualisation Group of the [Environmental Informatics](https://www.ufz.de/envinf/) department is a small team of researchers, focussing on the tasks of preparing and combining heterogenous data sets for visualisation in a unified context. The current team consists of:" >}}

{{< one-team-member
	name="Karsten Rink"
	email="karsten.rink@ufz.de"
	website="https://www.ufz.de/index.php?en=38483"
	image="team/karsten.jpg"
	anchor="anchor-karsten"
	bioStart="Karsten Rink studied Computer Science at the University of Leipzig, Germany. From 2003 to 2009 he worked"
	bioEnd=" as a research assistant at the University of Magdeburg, Germany, in the fields of image analysis and computer vision. He received his PhD in Visual Computing on the topic of improvements on implicit active contour methods for image segmentation. Since 2009 Karsten is working as a postdoctoral fellow at the Helmholtz-Centre for Environmental Research. His research interests include the integration, verification and visualisation of geoscientific data and he is a co-organiser of the annual EuroVis workshop on “Visualization in Environmental Sciences”. Since 2020 he is the head of the Data Integration and Visualisation Group. He is also a software engineer for the OpenGeoSys Data Explorer and the OpenGeoSys simulation software."
	>}}

{{< one-team-member
	name="Lars Bilke"
	email="lars.bilke@ufz.de"
	website="https://www.ufz.de/index.php?en=38480"
	image="team/lars.jpg"
	anchor="anchor-lars"
	bioStart="Lars Bilke has been a staff member of the Department of Environmental Informatics since 2008. He studied"
	bioEnd= " Media Informatics focussing on computer graphics, visualization and software development at the Leipzig University of Applied Sciences, Germany from 2003 to 2009 and started as an intern at the UFZ in 2007. He is head of the Visualization Center of the UFZ since 2013 where his research interests are related to scientific 3D visualization and virtual reality systems and interaction techniques. As the contact person for the Visualization Center he is involved in multiple research projects, helping scientists to benefit from applied visualization techniques to foster a better understanding of scientific questions. As part of the OpenGeoSys developer team he focusses on software and build engineering for multi-platform open-source software systems."
	>}}

{{< one-team-member
	name="Özgür Ozan Şen"
	email="oezguer-ozan.sen@ufz.de"
	image="team/ozan.jpg"
	anchor="anchor-ozan"
	bioStart="Özgür Ozan Şen graduated from Computer Engineering at Cankaya University in Ankara"
	bioEnd= ", Turkey as a Bachelor in 2011. He granted his Master’s degree in 2015 from Computer Animation and Game Technologies MSc program of Hacettepe University. Meanwhile, he worked as a software developer at different software companies in Ankara. He had been a research assistant in the Computer Engineering departmant at the University of Turkish Aeronautical Association before he started at the UFZ in 2020. His MSc thesis in game technologies was on the topic of 'Promotion Video Construction Of A Game Developed Using Unity 3D Game Engine'. Now, he is working as a scientist at the department of Environmental Informatics. His research interests are game technologies, virtual reality, data visualization and computer graphics."
	>}}

{{< one-team-member
	name="Nico Graebling"
	email="nico.graebling@ufz.de"
	image="team/nico.jpg"
	anchor="anchor-nico"
	bioStart="Nico Graebling studied Computer Science at the University of Leipzig. He graduated as Master of Science"
	bioEnd= " in 2018. The focus of his academic studies has been visualisation and computer graphics. Working in the Image and Signal Processing Group of Leipzig University as a research assistant Nico Graebling specialised in Augmented and Virtual Reality systems. He started in 2021 at the UFZ in the Department of Environmental Informatics as a research assistant. There, Nico Graebling develops interactive 3D visualisations in Unity for example for the Mont Terri underground research laboratory. His current research interests are interaction design and visualisations in Virtual Reality for the geoscientific domain."
	>}}

{{< one-team-member
	name="Julian Heinze"
	email="julian.heinze@ufz.de"
	image="team/julian.jpg"
	anchor="anchor-julian"
	bioStart="Julian Heinze studied Physics at RWTH Aachen University and graduated as Master of Science"
	bioEnd= " in 2022. He focused on Quantum Technologies in his studies, but switched to environmental research when he wrote his master's thesis at the UFZ Department of Ecological Modeling on 'Lattice Models for the Simulation of Species Distribution in Neotropical Rainforest'. In 2023, he started as a research assistant in the Department of Environmental Informatics. Since then, he has been working in the OpenWorkflow project on preparing data for simulations with the OpenGeoSys (OGS) simulation software."
	>}}

{{< one-team-member
	name="Susann Goldstein"
	email="susann.goldstein@ufz.de"
	image="team/susann.jpg"
	anchor="anchor-susann"
	bioStart="Susann Goldstein originally studied history before joining the group as a research assistant in 2023."
	bioEnd= "Susann's work focusses on data enquiry and data management, GIS data preprocessing as well as editorial work and literature research."
	>}}
	
{{< one-team-member
	name="Markus Jahn"
	email="markus.jahn@ufz.de"
	image="team/markus.jpg"
	anchor="anchor-markus"
	bioStart="Markus Jahn studied Applied System Sciences with application in geoinformatics at the university of Osnabrück."
	bioEnd= "From 2017 to 2021 he did his PhD at the Geodetic Institute at the KIT and wrote his theses on 'Distributed and Parallel Data Management to Support Geo-Scientific Simulation Implementations'. Since 2021 he is working as a scientific assistant at the KIT-GIK focussing on the Distributed Simulation of Processes in Buildings and City Models and is designing and implementing spatio-temporal models in DB4GeOGraphS to support distributed simulation processing based on spatio-temporal property graphs. In addition to this work, he has joined the group in 2024 to implement workflows for the 3D visualision of geoscientific data using data management systems."
	>}}
	
Previous team members:

{{< one-team-member
	name="Melanie Althaus"
	email=""
	image="team/melanie.jpg"
	anchor="anchor-melanie"
	bioStart="Melanie Althaus supported the group from 2022 to 2023 as a research assistant."
	bioEnd= "She has degrees in japanese studies and social studies and supported the group regarding a study evaluating science communication using 3D visualisation, researching techniques and designing questionaires."
	>}}
