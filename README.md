# Website Visgroup

## Getting Started for Development

1. Install [hugo](https://gohugo.io/installation/) (extended edition)
2. Clone this repository
3. Open the terminal and navigate into the folder "visgroup"
4. Start the built-in live server via "hugo server"
5. Use a browser to open the page specified in the server's output

## Content Editing

### Rule of Thumb

If you only want to edit existing content or add new content of the same structure, it should not be neccessary to write html-stuff. Just edit the .md files and use the same structures and syntax as already existing.

### Main Pages

The main pages like the landing page and all subpages can be found and edited inside the "visgroup/content"-folder. In order to create the projects-subpages (e.g. for urban systems, energy and geotechnics, and climate and ecosystems), one might want to copy the file "projects_hydro.md" and use it as a base for editing.

### Images

Images must be placed in subfolders of "visgroup/static". Please use (and if neccessary extend) the already existing structure instead of putting all images directly in the "static"-folder itself!

- **visgroup/static/previews**: images used on the landing page (_index.md)
- **visgroup/static/team**: images of the team
- **visgroup/static/general**: general images used for all/multiple pages
- **visgroup/static/project_categories**: thumbnail-images for project CATEGORIES (e.g. urban systems) used on the projects.md overview page
- **visgroup/static/projects_***CAT* **:** images of the projects of the category *"CAT"* (i.e. "projects_hydro" contains all images used for the page projects_hydro.md)

Please use the 'figureRelURL'-shortcode if you want to include images directly in Markdown. An example can be found in the page on Data Integration. 

## Publications

To add, edit or remove a publication do the following: 

1. Search for 'Rink, K' in the [UFZ Publication Index](https://www.ufz.de/index.php?en=20939)
2. Export the results as RIS
3. Import this file into you preferred publication management tool (e.g. Mendeley), sort it by date, and export it as .bib file
4. Edit the file "publications.bib" in the folder "visgroup\data"
5. Make sure that the .bib file entries are sorted by date, starting with the most recent publication
6. Convert the bibfile to BibJson format, using for example [this tool](https://github.com/internaut/bibtex2bibjson)
7. Replace the file "publications.json" with the newly generated file and ... that's it!

Note: If you get the server error "Unmarshal failed", it might be necessary to manually set the encoding of the generated json-file to UTF-8.

## Authors and Acknowledgments

Design: Melanie Althaus

Implementation: Nico Graebling

## License
tbd

## Project status
in development