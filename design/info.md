# Info-Sheet

This document provides some notes and informations concerning the VIS Website design.

The current design is not a finished product. It is rather a starting point for how the website could look like in the end.

## Colors

Taken from UFZ Corporate Design. (Possible Transparency according to CD: 80% / 40% / 20%)

- Helmholtz-Blue: hsla(206, 100%, 31%, 1) / #005aa0
  used for h2 underline and links

- Gray: hsla(195, 10%, 39%, 1) / #5a696e
  used for footer bg-color (transparency 40%)
	
- Logo-Gradient:
	  1. Aqua-blue: hsla(190, 85%, 48%, 1) / #13c1e3
    1. Green: hsla(101,47%,49%,1) / #69b843

- Department-Orange: hsla(36, 100%, 59%, 1) / #ffac2e
  used for hover on link

Other colors:

- White: hsla(0, 0%, 100%, 1)

- Lightgray: hsla(0, 0%, 96%, 1)
  used for possible section bg-color

- Soft-black: hsla(206, 10%, 14%, 1)
  used for text, hamburger menu bars

- Kind-of-UFZ-Blue: hsla(206, 91%, 31%, 1)
  used for visited links

- Dark-Department-orange: hsla(240, 100%, 50%, 1)
  used for active link

- Shadow-Black: hsla(0, 0%, 25%, 1)
  used for hamburger menu box-shadow

- Lightblue: hsla(190, 85%, 48%, 0.12)
  used for possible section bg-color

- Lightgreen: hsla(101, 47%, 49%, 0.15)
  used for possible section bg-color

## Typography

Due to privacy reasons all fonts should be hosted locally.

Fonts:
  Headers, team names: "Kanit"
  Paragraphs, header subtitle: "Nunito Sans"

Font-weight:
  h1: 700
  h2-h5, team names: 600
  navigation links: 500
  other text: 400

Font size:
  Based on 16px in mobile view, 18px in desktop view.
  Scale: 1.250 (Major Third)

### Links and tools used

Header photo by Pawel Czerwinski auf Unsplash: https://unsplash.com/de/fotos/eybM9n4yrpE

Team pug photos by charlesdeluvio on Unsplash: https://unsplash.com/@charlesdeluvio 
  - https://unsplash.com/de/fotos/DziZIYOGAHc
  - https://unsplash.com/de/fotos/Mv9hjnEUHR4
  - https://unsplash.com/de/fotos/K4mSJ7kc0As

Impressum-Generator Recht24: https://www.e-recht24.de/

Matomo Analytics: https://matomo.org/

Color Code Converter: https://web-toolbox.dev/en/tools/color-converter

Typography scale: https://typescale.com

Autoprefixer CSS online: https://autoprefixer.github.io